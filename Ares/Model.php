<?php

namespace Ares;

use \Ares\Database\Database;
use \Ares\Database\DatabaseRow;

class Model {

	private $attributes = array(),
	        $changes    = array();

	private static $table     = null,
	               $schema    = null,
	               $relations = null;

	const ONE_TO_ONE   = 1,
	      ONE_TO_MANY  = 2,
	      MANY_TO_MANY = 3;

	public static function find($id = null) {
		$class = get_called_class();
		if (!$class) {
		 	throw new Exception\ModelException("Cannot call find() directly on Model");
		}

		// If no id specified
		if ($id === null) {

			// Return a blank ActiveRecord
			return new ActiveRecord($class);

		// If an id has been set
		} else {

			// Create a new ActiveRecord with a where clause seaching for the id value
			$ar = new ActiveRecord($class);
			$ar->where('id = ?', $id);

			// Set id queries to prepared to enable caching of query
			$ar->prepare = true;

			// Return the first result if found
			return $ar->first();
		}
	}

	public function __construct($data = array()) {
		// $data = $this->filterInSchema($data);
		$this->changes = $data;
	}

	public function save($data=array()) {
		$data = $this->filterInSchema($data);
		$this->changes = array_merge(
			$this->changes,
			$data
		);

		$this->attributes = array_merge(
			$this->attributes,
			$this->changes
		);

		if (
			!isset($this->changes['id']) or
			!is_numeric($this->changes['id']) or
			$this->changes['id'] <= 0
		) {
			Database::create(
				new DatabaseRow(
					get_class($this),
					$this->changes
				)
			);

		} else {
			Database::update(
				new DatabaseRow(
					get_class($this),
					$this->changes
				)
			);
		}
	}

	public static function schema() {
		$class = get_called_class();

		$default = array(
			'id' => array(
				'type'           => 'int',
				'extra'          => 'unsigned',
				'auto_increment' => true,
				'key'            => 'primary',
				'null'           => false
			)
		);

		if (isset(self::$relations) and count(self::$relations) > 0) {
			foreach (self::$relations as $relation => $type) {
				switch ($type) {
					//case self::ONE_TO_ONE:
				}
			}
		}

		$schema = $class::$schema;

		$schema = array_merge(
			$default,
			$schema
		);

		return $schema;
	}

	function __set($name, $value) {
		if (isset($this->schema[$name])) {
			$this->changes[$name] = $value;
		} else {
			$this->$name = $value;
		}
	}

	function __get($name) {
		if (isset($this->changes[$name])) {
			return $this->changes[$name];
		}

		if (isset($this->attributes[$name])) {
			return $this->attributes[$name];
		}

		if (isset($this->$name)) {
			return $this->$name;
		}

		if (self::$relations) {

			$class = get_class();

			$relation = lcfirst($name);

			// If the relation is directly found
			if (isset(self::$relations[$relation])) {

				// Get the relationship
				$relationship = self::$relations[$relation];

				// One-to-One relationship
				if ($relationship == self::ONE_TO_ONE) {

					// Find the relation by the foreign key in this table
					$this->$name = $relation::findById($this->{$name.'_id'});

				} else {
					$this->$name = null;
				}

			} else {

				foreach (self::$relations as $relation => $values) {
					if (is_array($values)) {
						list($property, $relationship) = $values;
					} else {
						$relationship = $values;
						$property = $relation.'s'; // pluralise
					}

					// If this is the property being looked for
					if ($name == $property) {

						// One-to-Many relationship
						if ($relationship == self::ONE_TO_MANY) {
							$this->$name = $relation::find()->where($class::$table.'_id', $this->id);

						// Many-to-Many
						} elseif ($relationship == self::MANY_TO_MANY) {
							$this->$name = $relation::find()->leftJoin($class::$table.'_to_'.$relation::$table.' as c_to_r',
																	   'c_to_r.'.$class::$table.'_id = '.$this->id)
															->leftJoin($relation::$table.' as r', 'c_to_r.'.$relation::$table.'_id = r.id')
															->select('r.*');
						}
					}
				}

				// Look for ->plural
				$plural = (isset(self::$plural)) ? self::$plural : $class.'s';

				//if (isset(self::$relations[$relation])
			}

			return $this->$name;
		}

	}



	private function filterInSchema($data) {
		if (is_array($data)) {
			foreach ($data as $field => $attrs) {
				if (!isset(self::$schema[$field])) {
					unset($data[$field]);
				}
			}
			return $data;
		}
		return array();
	}

}