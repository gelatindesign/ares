<?php

namespace Ares;

class View {

	static $data = array(),
	       $template = 'default',
	       $view = null,
	       $args = array();

	static function render($args = null) {
		$args = (array) $args;
		extract((array) $args);

		self::$args = $args;

		require Config::$root . Config::$config['paths']['templates'] . self::$template . '.php';
	}

	static function view() {
		extract(self::$args);

		require Config::$root . Config::$config['paths']['views'] . self::$view . '.php';
	}

	static function partial($partial) {
		extract(self::$args);

		$path = Config::$root . Config::$config['paths']['partials'] . $partial . '.php';
		if (!is_file($path)) {
			throw new Exception\ViewException("Partial '" . $partial . "' not found in '" . $path . "'");
		}

		require Config::$root . Config::$config['paths']['partials'] . $partial . '.php';
	}

}