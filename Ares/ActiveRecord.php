<?php

namespace Ares;

use \Ares\Database\Database;
use \Ares\Database\DatabaseRelation;

class ActiveRecord implements \Iterator, \ArrayAccess, \Countable {

	public  $select        = array(),
	        $where         = '',
	        $where_params  = array(),
	        $order         = '',
	        $limit         = '',

	        $prepare       = false;

	private static $relations = null;


	public function __construct($relation) {
		$this->relation = $relation;

		if (Config::$env != 'production') {
			try {
				if (self::$relations == null) {
					self::$relations = Database::listRelations();
				}

				p(self::$relations);

				p($relation);

				if (!is_array(self::$relations) or !in_array($relation, self::$relations)) {
					Database::create(
						new DatabaseRelation(
							$relation,
							$relation::schema()
						)
					);
				}

				// Create the table if it does not exist
				//Database\Database::checkTable($model);

			} catch (Exception $e) {
				echo $e->getMessage();
				exit;
			}
		}
	}

	function select() {
		// Get the args
		$args  = func_get_args();

		// Get the query string
		$this->select = array_merge($this->select, $args);

		return $this;
	}

	/**
	 * Add a where clause to the ActiveRecord
	 *
	 * examples
	 *     ->where('foo = ?', 'bar');
	 *     ->where('foo = ? AND bar = ?', 1, 'yes');
	 *     ->where('foo = ? AND (bar = ? OR bar = ?)', 1, 'yes', 'maybe');
	 * 
	 * @return ActiveRecord $this
	 */
	public function where() {

		// Get the args
		$nargs = func_num_args();
		$args  = func_get_args();

		// Get the query string
		$query = array_shift($args);

		if ($query == 'AND' or $query == 'OR') {
			$join = $query;
			$query = array_shift($args);
		}

		// Check count of ? to params
		$count_q = substr_count($query, '?');

		if ($count_q != count($args)) {
			throw new Exception\ActiveRecordException("Incorrect number of arguments in: '" . $query . "', "
				. "'" . implode(',', $args)."'");
		}

		if ($this->where != '') {
			$this->where .= $join . " ";
		}

		$this->where .= $query;
		$this->where_params = array_merge($this->where_params, $args);

		return $this;
	}

	public function andWhere() {
		$this->where('AND');
	}

	public function orWhere() {
		$this->where('OR');
	}

	public function order($order) {
		$this->order = $order;
	}

	public function limit($limit) {
		$this->limit = $limit;
	}

	public function get($eq) {
		$this->fetchData();
		if (!isset($this->results[$eq])) {
			throw new Exception\ActiveRecordException("Result index '" . $eq . "' not valid in '" . $this->getQuery() . "'");
		}
		return $this->results[$eq];
	}

	public function first() {
		return $this->get(0);
	}


	private function getQuery() {

		// Only do this once
		if ($this->query == null) {

			$this->query = array();

			// Append select
			if (count($this->select)) {
				$this->query(

				);
			}
		}
	}

	private function fetchData() {
		$this->results = Database::read(
			$this->relation,
			$this
		);
	}


	/**
	 * Iterator Methods
	 */

	function current() {
		$this->fetchData();
		return $this->results[$this->offset];
	}

	function key() {
		return $this->offset;
	}

	function next() {
		++$this->offset;
	}

	function rewind() {
		$this->offset = 0;
	}

	function valid() {
		$this->fetchData();
		return isset($this->results[$this->offset]);
	}


	/**
	 * ArrayAccess Methods
	 */

 	function offsetExists($offset) {
		$this->fetchData();
 		return isset($this->results[$offset]);
		}
		
		function offsetGet($offset) {
		$this->fetchData();
			return $this->results[$offset];
	}

	function offsetSet($offset, $value) {
		$this->fetchData();
		if (is_null($offset)) {
			$this->results[] = $value;
		} else {
			$this->results[$offset] = $value;
		}
	}

	function offsetUnset($offset) {
		$this->fetchData();
		unset($this->results[$offset]);
	}


	/**
	 * Countable Methods
	 */
	
	function count() {
		$this->fetchData();
		return count($this->results);	
 	}

}