<?php

namespace Ares\Database;

class Database implements DatabaseCrud, DatabaseMeta {

	private static $protocol = null;

	public static function create($object) {
		$p = self::protocol();
		return $p::create($object);
	}

	public static function read($relation, $record = null) {
		$p = self::protocol();
		return $p::read($relation, $record);
	}

	public static function update($object) {
		$p = self::protocol();
		return $p::update($object);
	}

	public static function delete($object) {
		$p = self::protocol();
		return $p::delete($object);
	}

	public static function listRelations() {
		$p = self::protocol();
		return $p::listRelations();
	}


	private static function protocol() {
		if (self::$protocol === null) {
			$p = strtolower(\Ares\Config::$envvars['parameters']['database_protocol']);
			switch ($p) {
				case 'couchdb':
					self::$protocol = new CouchDB\CouchDB();
					break;

				case 'flatfile':
					self::$protocol = new FlatFile\FlatFile();
					break;

				case 'mysql':
				default:
					self::$protocol = new MySQL\MySQL();
					break;
			}
		}

		return self::$protocol;
	}

}