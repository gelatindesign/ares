<?php

namespace Ares\Database;

interface DatabaseMeta {

	public static function listRelations();

}