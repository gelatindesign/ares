<?php

namespace Ares\Database\MySQL;

class MySQL implements \Ares\Database\DatabaseCrud, \Ares\Database\DatabaseMeta {

	private static $conn      = null,              // connection object
	               $engine    = 'InnoDB',          // table engine type
	               $collation = 'utf8_unicode_ci', // table collation
	               $charset   = 'utf8';            // table charset


	/**
	 * Create an object, either a relation or a row
	 *
	 * @param  mixed $object DatabaseRelation or DatabaseRow to create
	 *
	 * @return bool
	 */
	public static function create($object) {
		$class = get_class($object);

		switch ($class) {
			case 'Ares\Database\DatabaseRelation':
				/**
				 * CREATE TABLE IF NOT EXISTS `foo` (
				 *   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				 *   `foo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
				 *   `bar` decimal(10,2) DEFAULT NULL,
				 *   PRIMARY KEY (`id`)
				 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
				 */

				$query = "CREATE TABLE IF NOT EXISTS `" . $object->name . "`";

				if ($object->schema !== null) {
					$query .= " (";

					$g_defaults = array(
						'length'         => false,
						'extra'          => false,
						'collation'      => false,
						'default'        => false,
						'null'           => true,
						'auto_increment' => false,
						'key'            => false
					);

					$primary = null;

					foreach ($object->schema as $field => $attributes) {
						$query .= "`" . $field . "` ";

						if (is_string($attributes)) {
							$attributes = array(
								'type' => $attributes
							);
						}

						$defaults = array();

						switch ($attributes['type']) {
							case 'int':
								$defaults = array(
									'length' => '11'
								);
								break;

							case 'decimal':
								$defaults = array(
									'length' => '10,2'
								);
								break;

							case 'varchar':
								$defaults = array(
									'length'    => '255',
									'collation' => self::$collation
								);
								break;

							case 'text':
								$defaults = array(
									'collation' => self::$collation
								);
								break;

							case 'date':
							case 'datetime':

								break;
						}

						$options = array_merge($g_defaults, $defaults, $attributes);

						$query .= $options['type'];

						if ($options['length'] !== false) {
							$query .= "(" . $options['length'] . ") ";

						} else {
							$query .= " ";
						}

						if ($options['extra'] !== false) {
							$query .= $options['extra'] . " ";
						}

						if ($options['collation'] !== false) {
							$query .= "COLLATE " . $options['collation'] . " ";
						}

						if ($options['null'] === true) {
							$query .= "NULL DEFAULT NULL";

						} else {
							if ($options['default'] !== false) {
								$query .= "DEFAULT '" . $options['default'] . "' ";
							}

							$query .= "NOT NULL ";
						}

						if ($options['key'] == 'primary' and $options['auto_increment'] === true) {
							$query .= "AUTO_INCREMENT ";

							$primary = $field;
						}

						$query = trim($query) . ",";
					}

					if ($primary !== null) {
						$query .= " PRIMARY KEY (`" . $primary . "`)";
					}

					$query = trim($query, ",") . ")";

					$query .= " ENGINE=" . self::$engine .
					          " DEFAULT CHARSET=" . self::$charset .
					          " COLLATE=" . self::$collation;
				}

				$result = self::conn()->query($query);

				break;

			case 'DatabaseRow':

				break;

			default:
				return false;
		}
	}

	public static function read($relation, $record = null) {

		// If no record set, select everything from the relation
		if ($record == null) {
			return self::conn()->query("SELECT * FROM " . $relation);
		}

		// Create the query
		$query = "SELECT ";

		// Check for select params
		if (count($record->select)) {
			$query .= implode(", ", $select) . " ";

		// If no select params specified, select all
		} else {
			$query .= "* ";
		}

		// Append the relation to the query
		$query .= "FROM " . $relation . " ";

		// Check for a where clause
		if (strlen($record->where)) {
			$query .= "WHERE " . $record->where;
		}

		if ($record->prepare) {
			$statement = self::conn()->prepare($query);
			$results   = self::conn()->execute($relation, $statement, $record->where_params);

		} else {
			$quoted_sql = self::conn()->quote($query, $record->where_params);
			$results    = self::conn()->query($quoted_sql, $relation);
		}

		return $results;
	}

	public static function update($object) {
		$class = get_class($object);

		switch ($class) {
			case 'Ares\Database\DatabaseRelation':

				break;

			case 'Ares\Database\DatabaseRow';
				if (
					!isset($this->values['id']) or
					!is_numeric($this->values['id']) or
					$this->values['id'] <= 0
				) {
					throw new DatabaseException();
				}

				$query = "UPDATE " . $object->relation . " SET";

				foreach ($values as $key => $val) {
					if ($key != 'id') {
						$query .= " `" . $key . "`" . "=:" . $key . ",";
					}
				}

				$query = trim($query, ",");

				$query .= " WHERE id = :id";

				$statement = self::conn()->prepare($query);
				$results   = self::conn()->execute($object->relation, $statement, $object->values);

				break;

			default:
				return false;
		}
	}

	public static function delete($object) {
		
	}


	public static function listRelations() {
		$query = "SHOW TABLES FROM " . self::conn()->getName();
		$results = self::conn()->query($query);
		return $results;
	}


	private static function conn() {
		if (self::$conn === null) {
			$c = \Ares\Config::$envvars['parameters'];

			self::$conn = new Connection(array(
				'host' => $c['database_host'],
				'port' => $c['database_port'],
				'name' => $c['database_name'],
				'user' => $c['database_user'],
				'pass' => $c['database_password']
			));
		}
		return self::$conn;
	}

}