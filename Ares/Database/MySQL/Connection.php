<?php

namespace Ares\Database\MySQL;

use Ares\Exception\DatabaseException;
use \PDO;

class Connection {

	private $host = null,
	        $port = null,
	        $name = null,
	        $user = null,
	        $pass = null,

	        $prepared_statements = array();

	/**
	 * Create the connection with options
	 * 
	 * @param array $options Options to create the connection with
	 */
	public function __construct($options) {
		foreach ($options as $key => $value) {
			$this->$key = $value;
		}

		// Try to get the pdo connection
		try {
			$this->pdo = new PDO("mysql:host=".$this->host.";dbname=".$this->name, $this->user, $this->pass);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch (PDOException $e) {
			throw new DatabaseException("MySQL\Connection failed to connect to the database");
		}
	}

	/**
	 * Prepare a query, keeping a local record of it to re-use if required matched against an md5 of the string
	 * 
	 * @param  string       $query Query to prepare
	 * @return PDOStatement        Prepared query
	 */
	public function prepare($query) {

		// Generate the query id
		$query_id = md5($query);

		// Check if the query has not yet been prepared
		if (!isset($this->prepared_statements[$query_id])) {

			// Get the PDOStatement for this query and store it against the query id
			$this->prepared_statements[$query_id] = $this->pdo->prepare($query);
		}

		// Return the stored statement
		return $this->prepared_statements[$query_id];
	}

	/**
	 * Execute a prepared statement
	 * 
	 * @param  string       $relation  Class name to instantiate each row as
	 * @param  PDOStatement $statement Statement to execute
	 * @param  array        $fields    Fields to execute with
	 * @return array                   Rows result
	 */
	public function execute($relation, $statement, $fields) {

		// Execute the statement with the fields
		$statement->execute($fields);

		// Check if the statement is a select query
		if (mb_substr($statement->queryString, 0, 6) == "SELECT") {

			// Instantiate each row as a class of $relation
			$rows = array();
			while ($r = $statement->fetch(PDO::FETCH_ASSOC)) {
				$rows[] = new $relation($r);
			}

			// Return rows
			return $rows;
		
		// If the statement is not a select query, return the statement as-is
		} else {
			return $statement;
		}
	}

	/**
	 * Quote a query with it's params
	 * 
	 * @param  string $query  Query to be quoted
	 * @param  array  $params Params to quote and pass into string
	 * @return string         Quoted string
	 */
	public function quote($query, $params) {
		$quoted_params = array();

		// Quote each param
		foreach ($params as $key => $value) {
			$quoted_params[] = $this->pdo->quote($value);
		}

		// Replace the '?' mark with the format specifier '%s'
		$query = str_replace('?', '%s', $query);

		// Return a replacement of %s with the params against the query
		return vsprintf($query, $quoted_params);
	}

	/**
	 * Run a query and return the results matched against the relation class
	 * 
	 * @param  string $relation Class to instantiate each row with
	 * @param  string $query    Query to run
	 * @return array            Results as array of $relation classes
	 */
	public function query($query, $relation = null) {

		if ($relation === null) {

			if (substr($query, 0, 6) == 'SELECT') {
				return $this->pdo->query($query, PDO::FETCH_ASSOC)->fetchAll();

			} else {
				return $this->pdo->query($query, PDO::FETCH_COLUMN, 0)->fetchAll();
			}

		// Return the result of the query fetched as the $relation
		} else {
			return $this->pdo->query($query, PDO::FETCH_CLASS, $relation)->fetchAll();
		}
	}


	/**
	 * Accessor Methods
	 */
	public function getHost() {
		return $this->host;
	}

	public function getPort() {
		return $this->port;
	}

	public function getName() {
		return $this->name;
	}

}