<?php

namespace Ares\Database;

interface DatabaseCrud {

	public static function create($object);
	public static function read($relation, $record = null);
	public static function update($object);
	public static function delete($object);

}