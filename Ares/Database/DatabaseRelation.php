<?php

namespace Ares\Database;

class DatabaseRelation {

	public $name   = null,
	       $schema = null;

	public function __construct($name, $schema = null) {
		$this->name   = $name;
		$this->schema = $schema;
	}

}