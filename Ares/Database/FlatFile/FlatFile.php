<?php

namespace Ares\Database\FlatFile;

class FlatFile implements \Ares\Database\DatabaseCrud {

	public static function create($object) {

	}

	public static function read($relation, $where = null, $params = null) {

		// If looking for a specific id, return the file object
		if ($where == 'id' and $params and count($params) == 1) {
			return (array) self::readJSON(strtolower($relation), $params[0]);
		}
	}

	public static function update($object) {

	}

	public static function delete($object) {

	}


	private static function readJSON($relation, $id = null) {
		if ($id == null) {

		} else {
			return json_decode(file_get_contents(
				\Ares\Config::$root . \Ares\Config::$config['paths']['database'] .
				strtolower($relation) . '.' . $id . '.json'
			));
		}
	}

}