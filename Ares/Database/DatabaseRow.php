<?php

namespace Ares\Database;

class DatabaseRow {

	public $relation   = null,
	       $values     = null;

	public function __construct($relation, $values) {
		$this->relation = $relation;
		$this->values   = $values;
	}

}