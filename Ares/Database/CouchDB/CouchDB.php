<?php

namespace Ares\Database\CouchDB;

class CouchDB implements \Ares\Database\DatabaseCrud {

	private static $conn = null;


	public static function create($object) {
		if (!isset($object->id) or empty($object->id)) {
			$object->id = self::getUniqueId();
		}

		self::conn()->send(
			"PUT",
			$object->id,
			self::encode($object)
		);
	}

	public static function read($id = "_all_docs", $params = null) {
		if (is_integer($id) or (is_string($id) and strpos($id, "WHERE") === false)) {
			$result = self::conn()->send(
				"GET",
				$id
			);

		} else {
			$map  = "function(doc) {";
			$map .= "if (";

			if (is_array($id)) {
				foreach ($id as $key => $value) {
					$map .= "doc." . $key . $value['comparison'] . "'" . $value['value'] . "'";
				}

			} else {
				$query = str_replace(array('WHERE', 'AND', 'OR'), array('', '&&', '||'), $id);

				$query = vsprintf(str_replace('?', "'%s'", $query), $params);

				$map .= $query;
			}

			$map .= ") {";
			$map .= "	emit(doc._id, doc);";
			$map .= "}";

			v($id, $map);

			$map = <<<MAP
$map
MAP;
			$view = '{"map":"' . $map . '"}';

			$result = self::conn()->send(
				"POST",
				$view
			);
		}

		return $result;
	}

	public static function update($object) {

	}

	public static function delete($object) {

	}


	private static function conn() {
		if (self::$conn === null) {
			$c = \Ares\Config::$envvars['parameters'];

			self::$conn = new Connection(array(
				'host' => $c['database_host'],
				'port' => $c['database_port'],
				'name' => $c['database_name'],
				'user' => $c['database_user'],
				'pass' => $c['database_password']
			));
		}
		return self::$conn;
	}

	private static function encode($object) {
		return json_encode($object);
	}

	private static function decode($json) {
		return json_decode($json);
	}

	private static function getUniqueId() {
		
	}

}