<?php

namespace Ares\Database\CouchDB;

use Ares\Exception\DatabaseException;

class Connection {

	private $host = null,
	        $port = null,
	        $name = null;

	/**
	 * Create the connection with options
	 * 
	 * @param array $options Options to create the connection with
	 */
	public function __construct($options) {
		foreach ($options as $key => $value) {
			$this->$key = $value;
		}
	}

	/**
	 * Send a request to a url with optional data
	 * 
	 * @param  string $method HTTP method to send with
	 * @param  string $url    Endpoint to send request to
	 * @param  string $data   Data to send to the endpoint
	 * @return string         Response body
	 */
	public function send($method, $url, $data = null) {

		// Open a socket
		$s = fsockopen($this->host, $this->port, $errno, $errstr);

		// If socket could not be opened, thrown an exception
		if (!$s) {
			throw new DatabaseException("CouchDB\Connection could not open socket: [" . $errno . "] '" . $errstr . "'");
		}

		// Build the request string
		$request = $method . " " . $url . " HTTP/1.0\r\nHost: " . $this->host . "\r\n";

		// Append user authentication if required
		if ($this->user) {
			$request .= "Authorization: Basic " . base64_encode($this->user . ":" . $this->pass) . "\r\n";
		}

		// Append data if required
		if ($data) {
			$request .= "Content-Length: " . strlen($data) . "\r\n\r\n";
			$request .= $data . "\r\n";
		} else {
			$request .= "\r\n";
		}

		// Write the request to the socket
		fwrite($s, $request);

		// Read the response
		$response = "";
		while (!feof($s)) {
			$response .= fgets($s);
		}

		// Extract the headers and body from the response
		list($this->headers, $this->body) = explode("\r\n\r\n", $response);

		// Return the response body
		return $this->body;
	}

}