<?php

function p() {
	$args = func_get_args();
	echo '<pre>';
	print_r($args);
	echo '</pre>';
}

function v() {
	$args = func_get_args();
	echo '<pre>';
	var_dump($args);
	echo '</pre>';
}

function simpleRegex($string) {

	// %+ => .+
	$string = str_replace('%+', '(.+)', $string);

	// % => .*
	$string = str_replace('%', '(.*)', $string);

	// :string => [A-Za-z_\-]+

	// :number => [0-9]+

	return '~' . $string . '~';
}

function renderHTML($obj) {
	$html = '';

	foreach ($obj as $element) {
		if (count($element) == 2) {
			list($tag, $inner) = $element;
		} else {
			list($tag, $inner, $attributes) = $element;
		}

		$html .= '<'.$tag;
		if (isset($attributes)) {
			foreach ($attributes as $key => $value) {
				$html .= ' '.$key.'="'.$value.'"';
			}
		}

		$html .= '>';
		if (is_array($inner)) {
			$html .= renderHTML($inner);
		} else {
			$html .= $inner;
		}
		$html .= '</'.$tag.'>' . "\n";
	}

	return $html;
}